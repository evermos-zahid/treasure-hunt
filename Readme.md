## Treasure Hunt 

### Pre-Requisite

- install golang : https://golang.org/doc/install

### How To Run

- move to directory treasure-hunt 
- run script `go run main.go`


### Output Program 

```
initialize grid
########
#......#
#.###..#
#...#.##
#X#....#
########
start position  4 1
Possible Path
[[3 1] [3 2] [3 3] [4 3]]
[[3 1] [2 1] [1 1] [1 2] [1 3] [1 4] [1 5] [2 5]]
[[3 1] [2 1] [1 1] [1 2] [1 3] [1 4] [1 5] [2 5] [3 5]]
[[3 1] [2 1] [1 1] [1 2] [1 3] [1 4] [1 5] [2 5] [3 5] [4 5]]
[[3 1] [2 1] [1 1] [1 2] [1 3] [1 4] [1 5] [1 6] [2 6]]
possible grid  1
########
#......#
#.###..#
#$$$#.##
#X#$...#
########
possible grid  2
########
#$$$$$.#
#$###$.#
#$..#.##
#X#....#
########
possible grid  3
########
#$$$$$.#
#$###$.#
#$..#$##
#X#....#
########
possible grid  4
########
#$$$$$.#
#$###$.#
#$..#$##
#X#..$.#
########
possible grid  5
########
#$$$$$$#
#$###.$#
#$..#.##
#X#....#
########
```
