package main
import (
	"fmt"
)

func printGrid(grid [][] string) {
	for i:=0;i<len(grid);i++ {
		for j:=0;j<len(grid[0]);j++ {
			fmt.Print(grid[i][j])
		}
		fmt.Println()
	}
}

func getStartPosition(grid [][] string) (x, y int) {
	indeksI :=0
	found := false
	for ( indeksI < len(grid) && !found) {
		indeksJ :=0
		for ( indeksJ < len(grid[0]) && !found) {
			if grid[indeksI][indeksJ] == "X" {
				found = true
				x = indeksI
				y = indeksJ
			}
			indeksJ++
			
		}
		indeksI++
	}
	return x,y
}

func getPosibilityPath(grid [][] string, x int, y int) (output [][][] int) {
	indeksI := x - 1
	var pathI [][] int
	var possiblePath [][][] int
	for ( indeksI >= 0 && grid[indeksI][y] == ".") {
		var newPathI = [] int {indeksI,y}
		pathI = append(pathI,newPathI)
		indeksJ := y+1
		var pathJ [][] int
		for ( indeksJ < len(grid[0]) && grid[indeksI][indeksJ] == ".") {
			var newPathJ = [] int {indeksI,indeksJ}
			pathJ = append(pathJ,newPathJ)
			indeksK :=indeksI+1
			var pathK [][] int
			for (indeksK < len(grid) && grid[indeksK][indeksJ] == ".") {
				var union = pathI
				for i:=0;i<len(pathJ);i++ {
					union = append(union,pathJ[i])
				}
				var newPathK = [] int {indeksK,indeksJ}
				pathK = append(pathK,newPathK)
				for i:=0;i<len(pathK);i++ {
					union = append(union, pathK[i])
				}
				possiblePath = append(possiblePath, union)
				indeksK++
			}
			indeksJ++
		}
		indeksI--
	}
	return possiblePath
}

func printGridPossible(grid [][] string,possible [][][] int) {
	for i:=0;i<len(possible);i++ {
		newGrid := grid
		var pathGrid = possible[i]
		for j:=0;j<len(pathGrid);j++ {
			newGrid[pathGrid[j][0]][pathGrid[j][1]] = "$"
		}
		fmt.Println("possible grid ", i+1)
		printGrid(newGrid)
		grid = clearGrid(newGrid)
		
	}
}

func clearGrid(grid [][] string)(output [][] string) {
	for i:=0;i<len(grid);i++ {
		for j:=0;j<len(grid[0]);j++ {
			if (grid[i][j] =="$"){
				grid[i][j] = "."
			}
		}
	}
	return grid
}

func main() {
	// initialize grid
	var grid = [][]string {
		{"#","#","#","#","#","#","#","#"},
		{"#",".",".",".",".",".",".","#"},
		{"#",".","#","#","#",".",".","#"},
		{"#",".",".",".","#",".","#","#"},
		{"#","X","#",".",".",".",".","#"},
		{"#","#","#","#","#","#","#","#"}}

	fmt.Println("initialize grid")
	printGrid(grid)

	// get starting position
	var x,y = getStartPosition(grid)
	fmt.Println("start position ",x,y)

	// get possible path with specific order, up, right and down

	var possible = getPosibilityPath(grid,x,y)
	fmt.Println("Possible Path")
	for i:=0;i<len(possible);i++ {
		fmt.Println(possible[i])
	}

	printGridPossible(grid,possible)
}